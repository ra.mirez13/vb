//
//  Extension + TextField.swift
//  electionsA2Lab
//
//  Created by Konstantin Chukhas on 1/25/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import Foundation
import  UIKit

extension UITextField{
    func setPadding (){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setBottomBorder(){
        self.layer.shadowColor = UIColor.red.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        
    }
    
}
