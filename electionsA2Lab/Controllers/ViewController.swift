//
//  ViewController.swift
//  electionsA2Lab
//
//  Created by Konstantin Chukhas on 1/25/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var demoButton: UIButton!
   var loginModel = LoginModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFields()
        ButtonLayer()
      }
    
    
    fileprivate func textFields() {
        emailTextField.setPadding()
        emailTextField.setBottomBorder()
        passwordTextField.setPadding()
        passwordTextField.setBottomBorder()
    }
    
    fileprivate func ButtonLayer() {
        enterButton.layer.cornerRadius = 5
        enterButton.clipsToBounds = true
        enterButton.layer.borderWidth = 1
        enterButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        demoButton.layer.cornerRadius = 5
        demoButton.clipsToBounds = true
        demoButton.layer.borderWidth = 1
        demoButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        loginModel.login(params: [
            "username":emailTextField.text!,
            "password":passwordTextField.text!
        ]) {
            print("Done")
        }
    }
    
    
}

