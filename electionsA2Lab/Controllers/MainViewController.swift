//
//  MainViewController.swift
//  electionsA2Lab
//
//  Created by Konstantin Chukhas on 1/25/19.
//  Copyright © 2019 Konstantin Chukhas. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    var header = ["Початок голосування","Явка виборців на дану годину","Завершення голосування","Внести інформацію з протоколу ДВК","Повідомити про порушення","Нормативно - правові документи","Навчання","Мої дані"]
     var des = ["Внести інформацію про початок голосування","Внести інформацію про явку виборців на дану годину","Внести інформацію про завершення голосування","Внести результати виборів і сфотографувати протокол","Швидко, просто, оперативно","Зразки заповнення документів(актів,протоколів та юридичні нормативи)","Відео щоб швидко навчатися користуватися системою",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}
extension MainViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return header.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MainTableViewCell
        
        cell?.headerLabel.text = header[indexPath.row]
        cell?.descriptionLAbel.text = des[indexPath.row]
        
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("selected \(indexPath.row)")
        case 1:
            print("selected \(indexPath.row)")
        case 2:
            print("selected \(indexPath.row)")
        case 3:
            print("selected \(indexPath.row)")
        case 4:
            print("selected \(indexPath.row)")
        case 5:
            print("selected \(indexPath.row)")
        case 6:
            print("selected \(indexPath.row)")
        case 7:
            print("selected \(indexPath.row)")
        default:
            break
        }
    }
    
    
}
